# YAWIK Joblist Widget

Display job postings of a yawik [jobwizard](https://jobwizard.yawik.org) instance on your website.

## Demo

If you are interested in displaying job postings of the [Yawik jobboard](https://jobboard.yawik.org) on your Website,
please visit the demo site: https://jobwizard.yawik.org/docs/widget/

## Development / Contributing

### Setup development

Checkout the repository, then run
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn dev
```

### Compiles and minifies for production

This section describes the processes to build and distribute the production files
and is targeted mainly at maintainers of this project.

```
yarn build
```

The demo site is hosted as a Github page from the branch `gh-pages`.
To distribute a new release, checkout this branch and put the files
in a folder named after the version under the `dist` directory.

Edit `index.md`, and add the files to the `Files` section.   
Also change the file sources in the `Example` section.

